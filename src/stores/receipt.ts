import { ref, computed, watch } from 'vue'
import { defineStore } from 'pinia'
import type { ReceiptItem } from '@/types/ReceiptItem'
import type { Receipt } from '@/types/Receipt'
import { useAuthStore } from './auth'

export const useReceiptStore = defineStore('receipt', () => {
  const authStore = useAuthStore()
  const receiptItems = ref<ReceiptItem[]>([])

  const receipt = ref<Receipt>({
    id: 0,
    createdDate: new Date(),
    total: 0,
    amount: 0,
    change: 0,
    paymentType: '',
    user: authStore.getCurrentUser() ?? undefined,
    memberId: -1,
    totalBefore: 0
  })

  watch(
    receiptItems,
    () => {
      calReceipt()
    },
    { deep: true }
  )

  function calReceipt() {
    let totalBefore = 0
    for (const item of receiptItems.value) {
      totalBefore = totalBefore + item.price * item.unit
    }
    receipt.value.totalBefore = totalBefore

    receipt.value.total = totalBefore
  }

  const addReceiptItem = (newReceiptItem: ReceiptItem) => {
    receiptItems.value.push(newReceiptItem)
  }
  const deleteReceiptItem = (selectedItem: ReceiptItem) => {
    const index = receiptItems.value.findIndex((item) => item === selectedItem)
    receiptItems.value.splice(index, 1)
  }
  const incUnitOfReceiptItem = (selectedItem: ReceiptItem) => {
    selectedItem.unit++
    calReceipt()
  }
  const decUnitOfReceiptItem = (selectedItem: ReceiptItem) => {
    selectedItem.unit--
    if (selectedItem.unit === 0) {
      deleteReceiptItem(selectedItem)
    }
  }
  const removeItem = (item: ReceiptItem) => {
    const index = receiptItems.value.findIndex((ri) => ri === item)
    receiptItems.value.splice(index, 1)
  }
  return {
    receipt,
    receiptItems,
    addReceiptItem,
    incUnitOfReceiptItem,
    decUnitOfReceiptItem,
    deleteReceiptItem,
    removeItem
  }
})
