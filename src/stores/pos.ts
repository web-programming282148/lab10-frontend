import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import { useLoadingStore } from './loading'
import productService from '@/services/product'
import { useMessageStore } from './message'
import type { Product } from '@/types/Product'

export const usePosStore = defineStore('pos', () => {
  const loadingStore = useLoadingStore()
  const messageStore = useMessageStore()

  const productsType1 = ref<Product[]>([])
  const productsType2 = ref<Product[]>([])
  const productsType3 = ref<Product[]>([])

  async function getProducts() {
    try {
      loadingStore.doLoad()
      const res1 = await productService.getProductByType(1)
      productsType1.value = res1.data
      const res2 = await productService.getProductByType(2)
      productsType2.value = res2.data
      const res3 = await productService.getProductByType(3)
      productsType3.value = res3.data
      loadingStore.finish()
    } catch (error: any) {
      messageStore.showMessage(error.message)
      loadingStore.finish()
    }
  }

  return { getProducts, productsType1, productsType2, productsType3 }
})
